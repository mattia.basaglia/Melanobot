Melanobot v2
============
This is the discontinued Melanobot v1. See https://gitlab.com/mattia.basaglia/Melanobot_v2 for the current incarnation of the project.


Melanobot v1
============

An IRC bot written in PHP

Contacts
--------
Mattia Basaglia <mattia.basaglia@gmail.com>


License
-------
AGPLv3 or later, see COPYING.


Sources
-------

Sources are available at https://gitlab.com/mattia.basaglia/Melanobot

Installing
==========

Requirements
------------

It requires PHP, a default installation should be enough.

Setting up a bot instance
-------------------------

Create a file called **setup-bot.php**, see /examples for some examples, 
then run **run-bot.sh** or execute that file through PHP to start the bot.
